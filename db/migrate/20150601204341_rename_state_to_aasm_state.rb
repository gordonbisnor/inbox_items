class RenameStateToAasmState < ActiveRecord::Migration
  def change
  	rename_column :inbox_items, :state, :aasm_state
  end
end
