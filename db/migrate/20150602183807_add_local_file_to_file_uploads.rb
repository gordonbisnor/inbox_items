class AddLocalFileToFileUploads < ActiveRecord::Migration
  def change
  	add_column :file_uploads, :local_file, :string
  end
end
