require "rails_helper"


RSpec.describe InboxItems::InboxItemsController, type: :routing do
  routes { InboxItems::Engine.routes }

  it "routes to index" do
    expect(get: '/').
      to route_to(controller: "inbox_items/inbox_items", action: "index")
  end
  
  it "routes to edit" do
    expect(get: '/1/edit').
      to route_to(controller: "inbox_items/inbox_items", action: "edit", id: '1')
  end

  it "routes to update" do
    expect(patch: '/1').
      to route_to(controller: "inbox_items/inbox_items", action: "update", id: '1')
  end
  
  it "routes to destroy" do
    expect(delete: '/1').
      to route_to(controller: "inbox_items/inbox_items", action: "destroy", id: '1')
  end

  it "routes to file redirect" do
    expect(get: '/1/file_redirect').
      to route_to(controller: "inbox_items/inbox_items", action: "file_redirect", id: '1')
  end

end
