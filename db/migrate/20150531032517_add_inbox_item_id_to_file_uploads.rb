class AddInboxItemIdToFileUploads < ActiveRecord::Migration
  def change
    add_column :file_uploads, :inbox_item_id, :integer
  end
end
