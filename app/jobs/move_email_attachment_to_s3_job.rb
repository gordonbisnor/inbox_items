module InboxItems
	class MoveEmailAttachmentToS3Job < ActiveJob::Base
	  queue_as :default
	 
	  def perform(file_upload)
	  	InboxItems::MoveFileUploadToS3.call(file_upload)
	  end
	end
end

