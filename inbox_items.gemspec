$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "inbox_items/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "inbox_items"
  s.version     = InboxItems::VERSION
  s.authors     = ["Gordon B. Isnor"]
  s.email       = ["gordonbisnor@gmail.com"]
  s.homepage    = "http://github.com/gordonbisnor/inbox_items"
  s.summary     = "Inbox items engine"
  s.description = "Inbox items engine"
  s.license     = "NO LICENSE"

  s.files = Dir["{app,config,db,lib}/**/*", "NO-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "< 6.1"
  s.add_dependency 'aasm'
  s.add_dependency 'charlock_holmes', '~> 0.7'

  s.add_development_dependency "sqlite3"
end
