InboxItems::Engine.routes.draw do

  resources :inbox_items, path: '/' do
  	member do
  		get :file_redirect
  	end
  end
  
end
