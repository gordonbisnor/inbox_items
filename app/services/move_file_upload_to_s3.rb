module InboxItems

	class MoveFileUploadToS3

		def self.call(file_upload)
			
			path = "file_uploads/#{file_upload.id}_#{file_upload.created_at.to_i}/#{file_upload.filename}"
			
			s3file = Amazon.put({file: file_upload.local_file, path: path, bucket: Settings.s3_bucket})
			
			if s3file
			  url = s3file.url_for(:read, {secure: false}).to_s.split('?')[0]
			  file_upload.update_attributes(url: url, path: path)
			  File.unlink(file_upload.local_file)
			end

		end

	end
	
end


