var logItemsReady = function () {

	$('input[name="inbox_item[convert]"]').on('switchChange.bootstrapSwitch', function (event, state) {
		var target = $(this).data('target');
		$(target).toggleClass('invisible');
	});

};

$(document).ready(logItemsReady);
$(document).on('page:load', logItemsReady);
