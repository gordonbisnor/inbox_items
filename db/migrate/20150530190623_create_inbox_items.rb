class CreateInboxItems < ActiveRecord::Migration
  def change
    create_table :inbox_items do |t|
      t.string :subject
      t.text :body
      t.integer :user_id
      t.integer :log_entry_id
      t.string :state
      t.timestamps null: false
    end
    add_index :inbox_items, :user_id
  end
end
