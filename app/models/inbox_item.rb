require 'aasm'

class InboxItem < ActiveRecord::Base
  include AASM

	belongs_to :user
  belongs_to :log_entry
  has_many :file_uploads

  attr_accessor :convert, :project_id, :log_time, :log_type, :date
      
  aasm whiny_transitions: true  do
    state :unread, initial: true
    state :read
    state :converted
    event :mark_as_read do
      transitions from: :unread, to: :read
    end
    event :convert_to_log_item do
      transitions from: :read, to: :converted
      before do
        do_conversion
      end
    end
  end

  def do_conversion
    transaction do
      project = user.projects.find(project_id)

    	l = project.log_entries.build({
        user_id: user.id,
  			log_time: self.log_time,
        date: self.date,
        log_type: self.log_type,
  			task_name: self.subject,
  			task_description: self.body,
        technical_risks: project.technical_risks
  			})
      
  		l.save!
      update_attribute(:log_entry_id, l.id)
      file_uploads.update_all(log_entry_id: l.id)
    end
  end

  def self.unread
    where(aasm_state: 'unread')
  end
  
  def self.unconverted
    where(aasm_state: ['unread', 'read'])
  end

  def title
    subject
  end

end
