InboxItems
==========

Install
-------

Gemfile:

```
gem 'inbox_items', git: 'https://bitbucket.org/gordonbisnor/inbox_items' 
gem 'griddler'
gem 'griddler-sendgrid'
```

Terminal:
```
bundle exec rake db:migrate
```

config/routes.rb:

```
Rails.application.routes.draw do
  mount InboxItems::Engine, at: "/inbox"
  mount_griddler
end
```

config/initializers/griddler.rb:

```
Griddler.configure do |config|
  config.processor_class = InboxItems::EmailProcessor # CommentViaEmail
end
```

app/models/user.rb:

```
has_many :inbox_items
```

Sendgrid:

https://sendgrid.com/developer/reply

- Host: xxx.com
- URL: http://xxx.com/email_processor