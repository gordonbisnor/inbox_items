module InboxItems
	class InboxItemsController < ApplicationController

		def index
			@inbox_items = current_user.inbox_items.order(id: :desc).unconverted.all
		end

		def edit
			@inbox_item = current_user.inbox_items.find(params[:id])
			@inbox_item.mark_as_read! if @inbox_item.may_mark_as_read?
			@inbox_item.convert = true unless @inbox_item.converted?
			@inbox_item.log_type = 'lab_notes'
			@inbox_item.date = @inbox_item.created_at.to_date
			@projects = current_user.projects.not_archived.not_deleted.all.collect { |p| [p.project_name, p.id] }
			@inbox_item.project_id = @projects.first[1] if @projects.count == 1
		end

		def update
			@inbox_item = current_user.inbox_items.find(params[:id])
			
			if @inbox_item.update_attributes(item_params)
				@inbox_item.convert_to_log_item! if item_params[:convert] == '1'
				redirect_to inbox_items_path, notice: 'Updated'
			end
		end

		def destroy
			@inbox_item = current_user.inbox_items.find(params[:id])
			@inbox_item.destroy
			redirect_to inbox_items_path, notice: t(:inbox_item_was_removed)
		end
		
		def file_redirect
			@inbox_item = current_user.inbox_items.find(params[:id])
			@file_upload = @inbox_item.file_uploads.find(params[:file_upload_id])
			redirect_to @file_upload.public_url if @file_upload.url.present?
		end

		def item_params
			params.require(:inbox_item).permit(:subject, :body, :project_id, :log_time, :convert, :log_type, :date)
		end
		private :item_params

	end
end

	 
