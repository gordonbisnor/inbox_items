class EmailProcessor
  require 'charlock_holmes'

  def initialize(email)
    @email = email
    email_address = @email.from[:email]
    @user = User.get_user_by_email_or_alias(email_address)  
  end

  def process

    inbox_item = create_inbox_item
        
    @email.attachments.each_with_index do |attachment,i|
      begin
        file_upload = create_file_upload(attachment, inbox_item)
        InboxItems::MoveEmailAttachmentToS3Job.perform_later(file_upload) if file_upload
      rescue => e
        
        Rails.logger.info "Rescue from error at EmailProcessor attachments each with index line 17: #{e}"
        next  
      end
    end # end each attachment

  end # end process

  def create_inbox_item
    inbox_item = @user.inbox_items.new(subject: formatted_subject, body: @email.raw_body)
    inbox_item.save!
    return inbox_item
  rescue => e
    Rails.logger.info "Rescue at Email Processor create_inbox_item, line 28: #{e}"
    return nil
  end
  
  def formatted_subject
    transcoded @email.subject.gsub(/FWD:|Fwd:|Fw:|Re:|RE:/, '').strip
  end

  def formatted_body
    transcoded email.body.gsub(/Sent from my iPhone/, '')
  end

  def transcoded(content)
    detection = CharlockHolmes::EncodingDetector.detect(content)
    CharlockHolmes::Converter.convert content, detection[:encoding], 'UTF-8'
  end

  def directory
    File.join(Rails.root, "public", "system", "email_attachments")
  end

  def create_file_upload(attachment, inbox_item)
    raise "No inbox item" unless inbox_item.present?

    path = store_file_locally(attachment)
    
    file_upload = FileUpload.new({
      inbox_item_id: inbox_item.id, 
      filename: attachment.original_filename, 
      mime: attachment.content_type, 
      file_type: attachment.content_type.split('/')[0],
      local_file: path,
      path: path
    })
    file_upload.save
    return file_upload
  rescue => e
    Rails.logger.info "Rescue at Email Processor create_file_upload line 48: #{e}"
    return false
  end

  def store_file_locally(attachment)
    name = attachment.original_filename
    path = File.join(directory, name)
    File.open(path, "wb") { |f| f.write(attachment.read) }
    return path
  rescue => e
    Rails.logger.info "Rescue at EmailProcessor store_file_locally line 57: #{e}"
  end

end # end email process class



