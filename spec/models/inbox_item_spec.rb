require 'rails_helper'

RSpec.describe InboxItem, type: :model do
  it "should be valid from the factory" do
  	expect(FactoryGirl.create(:inbox_item).class).to equal(InboxItem)
  end
  
  describe "scopes" do
  	
  	before do
	  	@unread = FactoryGirl.create(:inbox_item)

  		@read = FactoryGirl.create(:inbox_item)
  		@read.update_attribute(:aasm_state, 'read')

  		@converted = FactoryGirl.create(:inbox_item)
  		@converted.update_attribute(:aasm_state, 'converted')
  	end

  	it "has unread scope" do
  	  expect(InboxItem.unread).to include(@unread)
  	  expect(InboxItem.unread).to_not include(@read)
  	  expect(InboxItem.unread).to_not include(@converted)
  	end
  	
  	it "has unconverted scope" do
  		expect(InboxItem.unconverted).to include(@unread)
  	  expect(InboxItem.unconverted).to include(@read)
  	  expect(InboxItem.unconverted).to_not include(@converted)
  	end
	
	end

end

