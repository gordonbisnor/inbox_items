# Changelog 

## 0.5.0

Do not include archive or deleted projects –– To take advantage of this release, application not have not_archived and not_deleted scopes on Project model

## 0.4.1

Supports email aliases - To take advantage of this release, application most have a User.get_user_by_email_or_alias(email_address) method
